const webpack = require("webpack");

module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'RoboEnglish',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Roboenglish.kz' },
    ],
    // link: [
    //   { rel: 'icon', type: 'image/png', href: '~/assets/apple-touch-icon.png' }
    // ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Global CSS
  */
  css: [
    '~/assets/style.css'
  ],
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },

  plugins: [
  { src: '@/plugins/myplugin.js' },
  { src: '@/plugins/animation.js', ssr: false }
  ],

  modules: [
    // Or if you have custom options...
    ['vue-scrollto/nuxt', { duration: 800 }],
  ],

  buildModules: [
    '@nuxtjs/tailwindcss'
  ]
}

